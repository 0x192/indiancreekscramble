# Indian Creek Scramble

## Rules

1. Each foursome will play 18 holes.
2. All players will hit from the same tee. The team captain will select the best drive and the team will play their second shot from there. This procedure will continue until the ball is holed out. 
3. A player's lie can be improved by one club length (_no closer to the hole_) except when on the green or in a hazard.
4. All putts must be holed out. **NO GIMMIES**
5. Each member of a team must have their tee shot used twice. **NO MULLIGANS**
6. In the event of a tie, a hole's number will be picked at random (via dice roll) and the team with the lowest score on that hole will be declared the winner. If there is a tie on that hole the process will continue until a winner is determined. 
7. Per hole bets are encouraged. Both between and within teams. 
